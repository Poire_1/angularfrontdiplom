import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { MaterialModule } from './material';
import { ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { appRoutingModule } from './app.routing';
import { JwtInterceptor} from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { DeaneryHomeComponent} from '@app/deaneryModule/deaneryHome/deaneryHome.component'
import { LoginComponent } from './login/login.component';
import { RegStudentComponent} from './deaneryModule/regStudent/regStudent.component';
import { RegGroupComponent} from "./deaneryModule/regGroup/regGroup.component";
import { GroupView as GroupViewComponent} from './deaneryModule/viewGroup/viewGroup.component'
import { DatePipe } from '@angular/common';
import { StudentView as StudentViewComponent} from '@app/deaneryModule/viewStudent/viewStudent.component'; 
import { AddDebt as AddDebtComponent} from '@app/deaneryModule/addDebt/addDebt.component'
import { AddDepartment as AddDepartmentComponent} from '@app/deaneryModule/addDepartment/addDepartment.component'
import { AddClass as AddClassComponent} from '@app/deaneryModule/addClass/addClass.component'
import { AddProf as AddProfComponent} from '@app/deaneryModule/addProf/addProf.component'
import { AddSpecialty as AddSpecialtyComponent} from '@app/deaneryModule/addSpecialty/addSpecialty.component'
import { AddInstitution as AddInstitutionComponent} from '@app/deaneryModule/addInstitution/addInstitution.component'
import { SendEmail as SendEmailComponent} from '@app/sendEmail/sendEmail.component'
import { CommonModule } from '@angular/common';  
import { StudentHomeDesktop as StudentHomeDesktopComponent} from '@app/studentModule/studentHome/desktop/studentHomeDesktop.component'
import { StudentHomeMobile as StudentHomeMobileComponent} from '@app/studentModule/studentHome/mobile/studentHomeMobile.component'
import { StudentHome as StudentHomeComponent} from '@app/studentModule/studentHome/studentHome.component'
import { StudentAboutMeMobileComponent} from '@app/studentModule/studentAboutMe/mobile/studentAboutMeMobile.component'
import { ChatMobileComponent} from '@app/chat/mobile/chatMobile.component'
import { MessageComponent}  from '@app/chat/message/message.component'
import { InstitutionView as InstitutionViewComponent} from '@app/deaneryModule/InstitusionView/institutionView.component'
import { AddNews as AddNewsComponent} from '@app/deaneryModule/addNews/addNews.component'

@NgModule({
  declarations: [
        AppComponent,
        DeaneryHomeComponent,
        LoginComponent,
        RegStudentComponent,
        RegGroupComponent,
        StudentViewComponent,
        GroupViewComponent, 
        AddDebtComponent,
        SendEmailComponent,
        StudentHomeComponent, 
        StudentHomeMobileComponent,
        StudentHomeDesktopComponent,
        StudentAboutMeMobileComponent,
        ChatMobileComponent,
        MessageComponent,
        AddDepartmentComponent,
        AddInstitutionComponent,
        InstitutionViewComponent,
        AddSpecialtyComponent,
        AddClassComponent,
        AddProfComponent,
        AddNewsComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    appRoutingModule 
  ],
  exports:
  [
    MaterialModule,
  ],
  providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        DatePipe
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    AddDebtComponent,
    SendEmailComponent  
  ]
})
export class AppModule { }
